#pragma once

#ifndef ACTION_H
#define ACTION_H

// Signal handling
#include <signal.h>

#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/ColorRGBA.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int32.h"
#include <cmath>
#include "nav_msgs/Odometry.h"
#include <tf/transform_datatypes.h>
#include "std_srvs/Empty.h"

#include "tf/transform_listener.h"
#include "tf/transform_broadcaster.h"
#include "message_filters/subscriber.h"
#include "tf/message_filter.h"

#define translation_speed_max 0.9// in m/s
#define rotation_speed_max (M_PI/3) // 60 degres
#define rotation_coefficient_max (M_PI/6) // 30 degres

#define error_translation_threshold 0.3// in m
#define safety_distance 0.2//in m

#define error_rotation_threshold (M_PI/9)// in radians = 20 degres

using namespace std;

class action
{
private:

    ros::NodeHandle n;

    // communication with one_moving_person_detector or person_tracker
    ros::Subscriber sub_goal_to_reach;

    // communication with odometry
    ros::Subscriber sub_odometry;

    // communication with obstacle_detection
    ros::Subscriber sub_obstacle_detection;

    // communication with cmd_vel to send command to the mobile robot
    ros::Publisher pub_cmd_vel;

    geometry_msgs::Point goal_to_reach;
    bool new_goal_to_reach;

    //pid for rotation
    float rotation_to_do, rotation_done;
    bool cond_rotation;
    float initial_orientation;// to store the initial orientation ie, before starting the pid for rotation control
    float current_orientation;// to store the current orientation: this information is provided by the odometer
    float error_rotation;
    float error_integral_rotation;
    float error_previous_rotation;
    float current_rotation_speed;
    float kpr, kir, kdr;

    //pid for translation
    float translation_to_do, translation_done;
    bool cond_translation;//boolean to check if we still have to translate or not
    geometry_msgs::Point initial_position;// to store the initial position ie, before starting the pid for translation control
    geometry_msgs::Point current_position;// to store the current position: this information is provided by the odometer
    float error_translation;
    float error_integral_translation;
    float error_previous_translation;
    float current_translation_speed;
    float kpt, kit, kdt;

    //combination between rotation and translation
    float coef_rotation, coef_translation;

    bool init_odom;

    bool init_obstacle_detected;
    bool new_obstacle_detected;
    geometry_msgs::Point obstacle_detected;

public:

    action();

//UPDATE
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    void update();

    void display_new_goal_to_reach();

    void compute_rotation(bool stop = true);
    void display_rotation();

    void compute_translation();
    void display_translation();

    void detect_obstacle();
    void display_obstacle();

    void combine_rotation_and_translation();
    void display_rotation_and_translation();

    void move_robot();

//CALLBACKS
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    void odomCallback(const nav_msgs::Odometry::ConstPtr& o);
    void goal_to_reachCallback(const geometry_msgs::Point::ConstPtr& g);
    void closest_obstacleCallback(const geometry_msgs::Point::ConstPtr& obs);

    float distancePoints(geometry_msgs::Point pa, geometry_msgs::Point pb);

};

#endif