cmake_minimum_required(VERSION 2.8.3)
project(follow_me)

## Find the required ROS dependencies
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
)

set(CMAKE_BUILD_TYPE Debug)  # Set the build type to Debug for easier debugging

set(LIB_PATH ${CMAKE_SOURCE_DIR}/follow_me/lib)
link_directories(${LIB_PATH})

## Include header files from the 'include' directory and ROS dependencies
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare C++ executables
add_executable(detection_node src/detection_node.cpp src/datmo.cpp)
add_executable(datmo_node src/datmo_node.cpp src/datmo.cpp)
add_executable(robot_moving_node src/robot_moving_node.cpp)

add_executable(action_node src/action_node.cpp)
add_executable(rotation_node src/rotation_node.cpp)
add_executable(obstacle_detection_node src/obstacle_detection_node.cpp)

## Link the executables to the required libraries
target_link_libraries(detection_node datmo ${catkin_LIBRARIES} ${LIB_PATH})  # Link detection_node with datmo and ROS libraries
target_link_libraries(datmo_node datmo ${catkin_LIBRARIES} ${LIB_PATH})  # Link datmo_node with datmo and ROS libraries
target_link_libraries(robot_moving_node ${catkin_LIBRARIES})  # Link robot_moving_node with ROS libraries

target_link_libraries(action_node action ${catkin_LIBRARIES} ${LIB_PATH})  # Link action_node with action library and ROS libraries
target_link_libraries(rotation_node action ${catkin_LIBRARIES} ${LIB_PATH})  # Link rotation_node with action library and ROS libraries
target_link_libraries(obstacle_detection_node ${catkin_LIBRARIES})  # Link obstacle_detection_node with ROS libraries

## Set the output directory for executables
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/../devel/lib/follow_me)
