#include "action.h"

// UPDATE: main processing
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void action::update()
{

    if ( init_odom && init_obstacle_detected && ( cond_translation || cond_rotation ) )
    { // wait for the initialization of odometer and detect_obstacle_node

        display_new_goal_to_reach();
        
        compute_rotation();
        display_rotation();

        compute_translation();
        display_translation();

        detect_obstacle();
        display_obstacle();

        combine_rotation_and_translation();
        display_rotation_and_translation();
        move_robot();

        // do not change these lines
        new_goal_to_reach = false;
        new_obstacle_detected = false;
    }
    else if ( !init_obstacle_detected )
        ROS_WARN("waiting for obstacle_detection_node");
            else if ( !init_odom )
                ROS_WARN("waiting for odometry");

} // update

int main(int argc, char **argv)
{

    ros::init(argc, argv, "action_node");

    ROS_INFO("(action_node) waiting for a /goal_to_reach");
    action bsObject;

    ros::Rate r(10);//this node is updated at 10hz

    while (ros::ok())
    {
        ros::spinOnce();
        bsObject.update();
        r.sleep();
    }

    ros::spin();

    return 0;
}
