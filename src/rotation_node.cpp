#include "action.h"

// UPDATE: main processing
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void action::update()
{

    if ( init_odom && cond_rotation )
    { // wait for the initialization of odometer and detect_obstacle_node
        
        display_new_goal_to_reach();

        compute_rotation();
        display_rotation();
        
        move_robot();

        //do not modify this line
        new_goal_to_reach = false;
    }
    else 
        if ( !init_odom )
            ROS_WARN("waiting for /odom");

} // update

int main(int argc, char **argv)
{

    ros::init(argc, argv, "rotation_node");

    ROS_INFO("(rotation_node) waiting for a /goal_to_reach");
    action bsObject;

    ros::Rate r(10);//this node is updated at 10hz

    while (ros::ok())
    {
        ros::spinOnce();
        bsObject.update();
        r.sleep();
    }

    ros::spin();

    return 0;
}
