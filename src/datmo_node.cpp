#include "datmo.h"

//UPDATE
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::update() 
{

    // we wait for new data of the laser and of the robot_moving_node to perform laser processing
    if ( new_laser && new_robot ) 
    {

        ROS_INFO("\n");
        ROS_INFO("New data of laser received");
        ROS_INFO("New data of robot_moving received");        

        // display field of view
        display_field_of_view();

        //detection of motion
        detect_motion();
        display_motion();

        // clustering
        perform_clustering(); // to perform clustering
        display_clustering();

        // detection of legs
        detect_legs(); // to detect legs using cluster
        display_legs();

        // detection of persons
        detect_persons(); // to detect persons using legs detected
        display_persons();
       
        //TO COMPLETE
        // When do we do detection and when do we do tracking ?
        //detect_a_moving_person();  
        //display_a_moving_detected_person();
        //initialize_tracking();
        //track_a_person();
        //display_a_tracked_person();

        /*do not change this part*/
        new_laser = false;
        new_robot = false;
        previous_robot_moving = current_robot_moving;       
        
    }
    else
    {
        if ( !init_laser )
            ROS_WARN("waiting for laser data: run a rosbag");
        else
            if ( !init_robot )
                ROS_WARN("waiting for robot_moving_node: rosrun follow_me robot_moving_node");
    }

}// update

int main(int argc, char **argv)
{

    ros::init(argc, argv, "datmo_node");

    ROS_INFO("waiting for datmo of a moving person");
    datmo bsObject;

    ros::Rate r(10);

    while (ros::ok())
    {
        ros::spinOnce();
        bsObject.update();
        r.sleep();
    }

    ros::spin();

    return 0;
}
