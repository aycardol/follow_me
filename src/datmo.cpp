#include "datmo.h"

// DETECT MOTION FOR LASER
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::store_background()
{

    // TO COMPLETE
    // store all the hits of the laser in the background table
    for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
        ;

} // store_background

void datmo::reset_motion()
{

    // TO COMPLETE
    // for each hit, we reset the dynamic table
    for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
        ;

} // reset_motion

void datmo::detect_current_motion()
{
    // TO COMPLETE
    // for each hit, compare the current range with the background to detect motion
    // we fill the table dynamic
    for (int loop_hit = 0; loop_hit < nb_beams; loop_hit++)
        ;

} // detect_simple_motion

void datmo::detect_motion()
{
    // TO COMPLETE
    // you should integrate store_background, reset_motion and detect_current_motion in this function

    if (!current_robot_moving)
    {
        // the robot is not moving then we can perform moving person detection
        // DO NOT FORGET to store the background but when ???
        if (!previous_robot_moving)
        {
            // the robot was not moving
        }
        else
        {
            // the robot was moving
        }
    }
    else
    {
        // the robot is moving
        // IMPOSSIBLE TO DETECT MOTIONS because the base is moving
        // what is the value of dynamic table for each hit of the laser ?
        if (!previous_robot_moving)
        {
            // the robot was not moving
        }
        else
        {
            // the robot was moving
        }
    }

} // detect_motion

// CLUSTERING FOR LASER DATA
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::perform_clustering()
{
    
    perform_basic_clustering();
    perform_advanced_clustering();

} // perform_clustering

void datmo::perform_basic_clustering()
{
    // TO COMPLETE
    // we perform the clustering as described in the lecture on perception
    // the data related to each cluster are stored in cluster_start, cluster_end and nb_cluster: see datmo.h for more details
    // use: float distancePoints(geometry_msgs::Point pa, geometry_msgs::Point pb) to compute the euclidian distance between 2 points

    nb_clusters = 0;
    for (int loop_hit = 1; loop_hit < nb_beams; loop_hit++)
    {
        // TO COMPLETE
        /*     if EUCLIDIAN DISTANCE between (the previous hit and the current one) is higher than "cluster_threshold"
                {//the current hit does not belong to the same cluster*/
                // to compute the euclidian distance use : float datmo::distancePoints(geometry_msgs::Point pa, geometry_msgs::Point pb)
    }

     //Dont forget to update the different information for the last cluster
    //...

} // perform_basic_clustering

void datmo::perform_advanced_clustering()
{
    // TO COMPLETE
    /* for each cluster, we update:
        - cluster_size to store the size of the cluster ie, the euclidian distance between the first hit of the cluster and the last one
        - cluster_middle to store the middle of the cluster
        - cluster_dynamic to store the percentage of hits of the current cluster that are dynamic*/
    // use: float distancePoints(geometry_msgs::Point pa, geometry_msgs::Point pb) to compute the euclidian distance between 2 points

    for (int loop_cluster = 0; loop_cluster < nb_clusters; loop_cluster++)
    {
        int start = cluster_start[loop_cluster];
        int end = cluster_end[loop_cluster];
    }

} // perform_advanced_clustering

int datmo::compute_nb_dynamic(int start, int end)
{
    //TO COMPLETE
    // return the number of points that are dynamic between start and end

    int nb_dynamic = 0;
 
    return (nb_dynamic);

} // compute_nb_dynamic

// DETECTION OF PERSONS
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::detect_legs()
{
    // TO COMPLETE
    // a leg is a cluster:
        // - with a size higher than "leg_size_min";
        // - with a size lower than "leg_size_max;
        // if more than "dynamic_threshold"% of its hits are dynamic the leg is considered to be dynamic
    // we update the array leg_cluster, leg_detected and leg_dynamic

    nb_legs_detected = 0;
    for (int loop=0; loop<nb_clusters; loop++)//loop over all the clusters
    {
        // TO COMPLETE
    }
   
} // detect_legs

void datmo::detect_persons()
{

    //TO COMPLETE
    // a person has two legs located at less than "legs_distance_max" one from the other
    // a moving person (ie, person_dynamic array) has 2 legs that are dynamic
    // we update the detected_person table to store the middle of the person
    // we update the person_dynamic table to know if the person is moving or not      

    nb_persons_detected = 0;
    for (int loop_leg_left = 0; loop_leg_left < nb_legs_detected; loop_leg_left++)
        for (int loop_leg_right = 1+loop_leg_left; loop_leg_right < nb_legs_detected; loop_leg_right++)
        {
            // TO COMPLETE
        }
    
} // detect_persons

void datmo::detect_a_moving_person()
{

    // TO COMPLETE for detection
    // we store the moving_detected_person in moving_detected_person
    // we update the boolean is_moving_detected_person
    
    is_moving_person_detected = false;
    for (int loop_persons = 0; loop_persons < nb_persons_detected; loop_persons++)
        if (person_dynamic[loop_persons])
        {
            //TO COMPLETE
        }
    if ( is_moving_person_detected )
        pub_detection.publish(moving_detected_person);

} // detect_a_moving_person

// TRACKING OF A PERSON
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
void datmo::track_a_person()
{

    was_person_tracked = is_person_tracked; //do not remove this line
    //see slides related to tracking in the datmo lecture
    float distance_associated;
    
    // association between the tracked person and the possible detection
    for (int loop_persons = 0; loop_persons < nb_persons_detected; loop_persons++)
    {
        // we search for the detected_person which is the closest one to the tracked_person
        // we store the related information in index_associated and distance_associated
        // associated = ...; TO COMPLETE: how do you know that the tracked person has been associated with one detected person ?    
    }
    
    if ( associated )
    {
        // update the information related to the tracked_person, frequency and uncertainty knowing that there is an association
        // should we publish or not tracked_person ?
    }
    else
    {
        // update the information related to the tracked_person, frequency and uncertainty knowing that there is no association
        // should we publish or not tracked_person ?
    }

    // do not forget to update tracked_person according to the current association

}

